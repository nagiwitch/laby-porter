
#^(http|https|ftp|file)://.*/(.+)

def get_filename_url(url)
	/([http|https|ftp|file]):\/\/.*\/(.+)/.match(url).try &.[2]
end

def get_packname(url)
	/(.+).tar./.match(get_filename_url(url)).try &.[1]
end
